use crate::app::AppMsg;
use crate::mqtt::{Format, QoS, FORMATS, QOS};
use relm4::factory::{DynamicIndex, FactoryComponent, FactorySender};
use relm4::gtk::prelude::EditableExt;
use relm4::{
    adw::{
        self,
        traits::{
            ComboRowExt, EntryRowExt, ExpanderRowExt, PreferencesGroupExt, PreferencesRowExt,
        },
    },
    gtk::{self, traits::ButtonExt},
};

#[derive(Debug)]

pub struct SubscribedTopics {
    pub name: String,
    pub format: Format,
    pub qos: QoS,
}

#[derive(Debug)]
pub enum SubscribedTopicsOutput {
    Remove(DynamicIndex),
}

#[derive(Debug)]
pub enum SubscribedTopicsMsg {
    UpdateName(String),
    UpdateFormat(Format),
    UpdateQoS(QoS),
}

#[relm4::factory(pub)]
impl FactoryComponent for SubscribedTopics {
    type Init = SubscribedTopics;
    type Input = SubscribedTopicsMsg;
    type Output = SubscribedTopicsOutput;
    type CommandOutput = ();
    type Widgets = SubscribedTopicsWidgets;
    type ParentInput = AppMsg;
    type ParentWidget = adw::PreferencesGroup;

    view! {
        root = &adw::PreferencesGroup {
            add = &adw::ExpanderRow {
                #[watch]
                set_title: &self.name,
                set_expanded: true,
                add_action = &gtk::Button::from_icon_name("edit-delete-symbolic") {
                    set_has_frame: false,
                    connect_clicked[sender, index] => move |_| {
                        sender.output(SubscribedTopicsOutput::Remove(index.clone()));
                    }
                },
                add_row = &adw::EntryRow {
                    set_title: "Topic",
                    set_text: &self.name,
                    set_show_apply_button: true,
                    connect_apply[sender] => move |entry| {
                        sender.input(SubscribedTopicsMsg::UpdateName(entry.text().to_string()));
                    }
                },
                add_row = &adw::ComboRow {
                    set_title: "QoS",
                    set_model: Some(&gtk::StringList::new(&QOS)),
                    connect_selected_item_notify[sender] => move |entry| {
                        sender.input(SubscribedTopicsMsg::UpdateQoS(QoS::from_u32(entry.selected())));
                    }
                },
                add_row = &adw::ComboRow {
                    set_title: "Format",
                    set_model: Some(&gtk::StringList::new(&FORMATS)),
                    connect_selected_item_notify[sender] => move |entry| {
                        sender.input(SubscribedTopicsMsg::UpdateFormat(Format::from_u32(entry.selected())));
                    }
                },
            },
        }
    }

    fn output_to_parent_input(output: Self::Output) -> Option<AppMsg> {
        Some(match output {
            SubscribedTopicsOutput::Remove(index) => AppMsg::RemoveSubscription(index),
        })
    }

    fn init_model(value: Self::Init, _index: &DynamicIndex, _sender: FactorySender<Self>) -> Self {
        Self {
            name: "New Topic".to_string(),
            format: Format::Text,
            qos: QoS::AtLeastOnce,
        }
    }

    fn update(&mut self, msg: Self::Input, _sender: FactorySender<Self>) {
        match msg {
            SubscribedTopicsMsg::UpdateQoS(qos) => self.qos = qos,
            SubscribedTopicsMsg::UpdateFormat(format) => self.format = format,
            SubscribedTopicsMsg::UpdateName(name) => self.name = name,
        }
    }
}
