use crate::{
    modals::about::AboutDialog,
    mqtt::{WorkerModel, WorkerMsg},
    subscribed_topics::SubscribedTopics,
};
use relm4::{
    actions::{ActionGroupName, RelmAction, RelmActionGroup},
    adw::{
        self,
        traits::{
            ComboRowExt, EntryRowExt, PreferencesGroupExt, PreferencesPageExt, PreferencesRowExt,
        },
    },
    factory::FactoryVecDeque,
    gtk::{
        self,
        traits::{ButtonExt, TextViewExt},
    },
    main_application,
    prelude::DynamicIndex,
    Component, ComponentController, ComponentParts, ComponentSender, Controller, SimpleComponent,
    WorkerController,
};
use std::convert::identity;

use crate::mqtt::{Format, QoS, QOS};
use gtk::prelude::{
    ApplicationExt, ApplicationWindowExt, BoxExt, EditableExt, GtkWindowExt, SettingsExt, WidgetExt,
};
use gtk::{gio, glib};

use crate::config::{APP_ID, PROFILE};

#[derive(Debug, Clone)]
pub struct ClientParams {
    pub port: String,
    pub url: String,
    pub client_id: String,
}

#[derive(Debug, Clone)]
pub struct Message {
    pub topic: String,
    pub text: String,
    pub qos: u32,
}

pub(super) struct App {
    client_params: ClientParams,
    about_dialog: Controller<AboutDialog>,
    mqtt_worker: WorkerController<WorkerModel>,
    page: String,
    send_message: Message,
    subscribers: FactoryVecDeque<SubscribedTopics>,
}

#[derive(Debug)]
pub enum AppMsg {
    Connect,
    Disconnect,
    UrlChanged(String),
    ClientIdChanged(String),
    PortChanged(String),
    TopicChanged(String),
    MessageChanged(String),
    QosChanged(u32),

    SendMessage,
    SwitchToMessageDialog,
    SwitchToConnectDialog,
    AddSubscription,
    RemoveSubscription(DynamicIndex),
    DisplayError(String),
    Quit,
}

relm4::new_action_group!(pub(super) WindowActionGroup, "win");
relm4::new_stateless_action!(PreferencesAction, WindowActionGroup, "preferences");
relm4::new_stateless_action!(pub(super) ShortcutsAction, WindowActionGroup, "show-help-overlay");
relm4::new_stateless_action!(AboutAction, WindowActionGroup, "about");

#[relm4::component(pub)]
impl SimpleComponent for App {
    type Init = ();
    type Input = AppMsg;
    type Output = ();
    type Widgets = AppWidgets;

    menu! {
        primary_menu: {
            section! {
                "_Preferences" => PreferencesAction,
                "_Keyboard" => ShortcutsAction,
                "_About MQTT Client" => AboutAction,
            }
        }
    }

    view! {
        main_window = gtk::ApplicationWindow::new(&main_application()) {
            connect_close_request[sender] => move |_| {
                sender.input(AppMsg::Quit);
                gtk::Inhibit(true)
            },

            #[name(shortcuts)]
            #[wrap(Some)]
            set_help_overlay = &gtk::Builder::from_resource(
                    "/org/tronta/mqttclient/gtk/help-overlay.ui"
                )
                .object::<gtk::ShortcutsWindow>("help_overlay")
                .unwrap() -> gtk::ShortcutsWindow {
                    set_transient_for: Some(&main_window),
                    set_application: Some(&main_application()),
            },

            add_css_class?: if PROFILE == "Devel" {
                    Some("devel")
                } else {
                    None
                },

            #[wrap(Some)]
            set_titlebar = &gtk::HeaderBar {
                pack_start = &gtk::Button::with_label("Connect") {
                    #[watch]
                    set_visible: model.page == "connect",
                    connect_clicked[sender] => move |_| {
                        sender.input(AppMsg::Connect);
                    }
                },
                pack_start = &gtk::Button::with_label("Disconnect") {
                    #[watch]
                    set_visible: model.page != "connect",
                    connect_clicked[sender] => move |_| {
                        sender.input(AppMsg::Disconnect);
                    }
                },
                pack_end = &gtk::MenuButton {
                    set_icon_name: "open-menu-symbolic",
                    set_menu_model: Some(&primary_menu),
                }
            },
            gtk::Stack {
                #[watch]
                set_visible_child_name: model.page.as_str(),
                add_child = &adw::PreferencesPage {
                    add = &adw::PreferencesGroup {
                        set_title: "Connection Settings",
                        add: entry_client_id = &adw::EntryRow {
                            set_title: "Client ID",
                            set_input_purpose: gtk::InputPurpose::Alpha,
                            set_text: &model.client_params.client_id,
                            connect_changed[sender] => move |entry| {
                                sender.input(AppMsg::ClientIdChanged(entry.text().to_string()));
                            }
                        },
                        #[name(entry_url)]
                        add = &adw::EntryRow {
                            set_title: "URL of MQTT Broker",
                            set_input_purpose: gtk::InputPurpose::Url,
                            set_text: &model.client_params.url,
                            connect_changed[sender] => move |entry| {
                                sender.input(AppMsg::UrlChanged(entry.text().to_string()));
                            }
                        },
                        #[name(entry_port)]
                        add = &adw::EntryRow {
                            set_title: "Port",
                            set_input_purpose: gtk::InputPurpose::Digits,
                            set_text: &model.client_params.port,
                            connect_changed[sender] => move |entry| {
                                // TODO: Improve Error Handling
                                sender.input(AppMsg::PortChanged(entry.text().to_string()));
                            }
                        },
                    }
                } -> {
                    set_name: "connect"
                },
                add_child = &adw::PreferencesPage {
                    add = &adw::PreferencesGroup {
                        set_title: "Logged Messages",
                        #[wrap(Some)]
                        set_header_suffix = &gtk::Box {
                            append = &gtk::Button::from_icon_name("edit-clear-all-symbolic") {
                                set_has_frame: false,
                            },
                            append = &gtk::Button::from_icon_name("document-save-symbolic") {
                                set_has_frame: false,
                            },
                            set_spacing: 5,
                        },
                            add = &gtk::TextView::new() {
                                set_height_request: 200,
                                set_editable: false,
                            }
                        },
                    add = &adw::PreferencesGroup {
                        set_title: "Send Message",
                        #[wrap(Some)]
                        set_header_suffix = &gtk::Button::from_icon_name("mail-send-symbolic") {
                            set_has_frame: false,
                            connect_clicked[sender] => move |_| {
                                sender.input(AppMsg::SendMessage);
                        }
                        },
                        add: topic = &adw::EntryRow {
                            set_title: "Topic",
                            set_input_purpose: gtk::InputPurpose::Alpha,
                            set_text: &model.send_message.topic,
                            connect_changed[sender] => move |entry| {
                                sender.input(AppMsg::TopicChanged(entry.text().to_string()));
                            }
                        },
                        add: message = &adw::EntryRow {
                            set_title: "Message",
                            set_input_purpose: gtk::InputPurpose::Alpha,
                            set_text: &model.send_message.text,
                            connect_changed[sender] => move |entry| {
                                sender.input(AppMsg::MessageChanged(entry.text().to_string()));
                            }
                        },
                        add: qos = &adw::ComboRow {
                            set_title: "QoS",
                            set_model: Some(&gtk::StringList::new(&QOS)),
                            set_selected: model.send_message.qos,
                            connect_selected_item_notify[sender] => move |entry| {
                                sender.input(AppMsg::QosChanged(entry.selected()));
                            }
                        },
                    },
                    #[local_ref]
                    add = subscriber_prefs -> adw::PreferencesGroup {
                        set_title: "Subscribed Topics",
                        #[wrap(Some)]
                        set_header_suffix = &gtk::Button::from_icon_name("list-add-symbolic") {
                            set_has_frame: false,
                            connect_clicked[sender] => move |_| {
                               sender.input(AppMsg::AddSubscription);
                            }
                        }
                    }
                } -> {
                    set_name: "messages",
                },
            }
        }
    }

    fn init(
        _init: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let about_dialog = AboutDialog::builder()
            .transient_for(root)
            .launch(())
            .detach();

        let message = Message {
            topic: "topic".to_string(),
            text: "Test".to_string(),
            qos: 1,
        };

        let subscribers =
            FactoryVecDeque::new(adw::PreferencesGroup::default(), sender.input_sender());

        let model = Self {
            subscribers,
            about_dialog,
            client_params: ClientParams {
                client_id: "mqtt_client".to_string(),
                url: "localhost".to_string(),
                port: "1883".to_string(),
            },
            send_message: message,
            page: "connect".to_string(),
            mqtt_worker: WorkerModel::builder()
                .detach_worker(())
                .forward(sender.input_sender(), identity),
        };

        let subscriber_prefs = model.subscribers.widget();

        let widgets = view_output!();

        let actions = RelmActionGroup::<WindowActionGroup>::new();

        let shortcuts_action = {
            let shortcuts = widgets.shortcuts.clone();
            RelmAction::<ShortcutsAction>::new_stateless(move |_| {
                shortcuts.present();
            })
        };

        let about_action = {
            let sender = model.about_dialog.sender().clone();
            RelmAction::<AboutAction>::new_stateless(move |_| {
                sender.send(()).unwrap();
            })
        };

        actions.add_action(&shortcuts_action);
        actions.add_action(&about_action);

        widgets
            .main_window
            .insert_action_group(WindowActionGroup::NAME, Some(&actions.into_action_group()));

        widgets.load_window_size();

        ComponentParts { model, widgets }
    }

    fn update(&mut self, message: Self::Input, _sender: ComponentSender<Self>) {
        let mut subscription_guard = self.subscribers.guard();

        match message {
            AppMsg::Connect => {
                self.mqtt_worker
                    .emit(WorkerMsg::Connect(self.client_params.clone()));
            }
            AppMsg::Disconnect => {
                self.mqtt_worker.emit(WorkerMsg::Disconnect);
            }
            AppMsg::ClientIdChanged(client_id) => {
                self.client_params.client_id = client_id;
            }
            AppMsg::UrlChanged(url) => {
                self.client_params.url = url;
            }
            AppMsg::PortChanged(port) => {
                self.client_params.port = port;
            }
            AppMsg::TopicChanged(topic) => {
                self.send_message.topic = topic;
            }
            AppMsg::MessageChanged(text) => {
                self.send_message.text = text;
            }
            AppMsg::QosChanged(qos) => {
                self.send_message.qos = qos;
            }
            AppMsg::SwitchToMessageDialog => self.page = "messages".to_string(),
            AppMsg::SwitchToConnectDialog => {
                self.page = "connect".to_string();
            }
            AppMsg::Quit => main_application().quit(),
            AppMsg::DisplayError(error) => {
                // TODO: Display Error as TOAST
                println!("Display Error Message: {error}");
            }
            AppMsg::SendMessage => {
                self.mqtt_worker
                    .emit(WorkerMsg::SendMessage(self.send_message.clone()));
            }
            AppMsg::AddSubscription => {
                subscription_guard.push_back(SubscribedTopics {
                    name: "New Topic".to_string(),
                    format: Format::Text,
                    qos: QoS::AtLeastOnce,
                });
            }
            AppMsg::RemoveSubscription(index) => {
                // TODO;  Here we actually need to cancel the subscription
                subscription_guard.remove(index.current_index());
            }
        }
    }

    fn shutdown(&mut self, widgets: &mut Self::Widgets, _output: relm4::Sender<Self::Output>) {
        widgets.save_window_size().unwrap();
    }
}

impl AppWidgets {
    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        let settings = gio::Settings::new(APP_ID);
        let (width, height) = self.main_window.default_size();

        settings.set_int("window-width", width)?;
        settings.set_int("window-height", height)?;

        settings.set_boolean("is-maximized", self.main_window.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        let settings = gio::Settings::new(APP_ID);

        let width = settings.int("window-width");
        let height = settings.int("window-height");
        let is_maximized = settings.boolean("is-maximized");

        self.main_window.set_default_size(width, height);

        if is_maximized {
            self.main_window.maximize();
        }
    }
}
