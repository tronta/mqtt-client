use crate::app::{AppMsg, ClientParams, Message};
use mqtt::{Client, Error};
use paho_mqtt as mqtt;
use relm4::{ComponentSender, Worker};
use std::{thread, time::Duration};

pub struct WorkerModel {
    client: Option<Client>,
}

pub const FORMATS: [&str; 2] = ["text", "json"];

#[derive(Debug)]
pub enum Format {
    Text,
    Json,
}

impl Format {
    pub fn from_u32(num: u32) -> Format {
        match num {
            0 => Format::Text,
            1 => Format::Json,
            n => panic!("Unexpected value: {n}"),
        }
    }
}

pub const QOS: [&str; 3] = ["0 - At most one", "1 - At least once", "2 - Exactly once"];

#[derive(Debug)]
pub enum QoS {
    AtMostOne = 0,
    AtLeastOnce = 1,
    ExactlyOnce = 2,
}

impl QoS {
    pub fn from_u32(num: u32) -> QoS {
        match num {
            0 => QoS::AtMostOne,
            1 => QoS::AtLeastOnce,
            2 => QoS::ExactlyOnce,
            n => panic!("Unexpected value: {n}"),
        }
    }
}

#[derive(Debug)]
pub enum WorkerMsg {
    Connect(ClientParams),
    SendMessage(Message),
    Disconnect,
}

impl Worker for WorkerModel {
    type Init = ();
    type Input = WorkerMsg;
    type Output = AppMsg;

    fn init(_init: Self::Init, _sender: ComponentSender<Self>) -> Self {
        Self { client: None }
    }

    fn update(&mut self, msg: Self::Input, sender: ComponentSender<Self>) {
        match msg {
            WorkerMsg::Connect(params) => match self.connect(&params) {
                Ok(_) => {
                    sender.output(AppMsg::SwitchToMessageDialog).unwrap();
                }
                Err(e) => {
                    sender
                        .output(AppMsg::DisplayError(format!("Error while connecting: {e}")))
                        .unwrap();
                }
            },
            WorkerMsg::Disconnect => match self.disconnect() {
                Ok(_) => {
                    sender.output(AppMsg::SwitchToConnectDialog).unwrap();
                }
                Err(e) => {
                    sender
                        .output(AppMsg::DisplayError(format!(
                            "Error while disconnecting: {e}"
                        )))
                        .unwrap();
                }
            },
            WorkerMsg::SendMessage(message) => match self.send(message) {
                Ok(_) => {
                    // TODO: What should we do when message was successfully sent?
                }
                Err(e) => {
                    sender
                        .output(AppMsg::DisplayError(format!(
                            "Error while sending an message: {e}"
                        )))
                        .unwrap();
                }
            },
        };
    }
}

impl WorkerModel {
    fn connect(&mut self, params: &ClientParams) -> Result<(), Error> {
        let uri = format!("tcp://{}:{}", params.url, params.port);
        let create_opts = mqtt::CreateOptionsBuilder::new()
            .server_uri(uri)
            .client_id(params.client_id.clone())
            .finalize();

        let lwt = mqtt::Message::new("error", "MQTT Client lost connection", mqtt::QOS_1);

        let conn_opts = mqtt::ConnectOptionsBuilder::new()
            .keep_alive_interval(Duration::from_secs(30))
            .mqtt_version(mqtt::MQTT_VERSION_3_1_1)
            .clean_session(false)
            .will_message(lwt)
            .finalize();

        thread::scope(|s| -> Result<(), Error> {
            let h = s.spawn(|| -> Result<(), Error> {
                // Create the client connection
                let client = mqtt::Client::new(create_opts)?;
                client.connect(conn_opts)?;
                self.client = Some(client);
                Ok(())
            });
            h.join().unwrap()
        })
    }

    fn disconnect(&mut self) -> Result<(), Error> {
        thread::scope(|s| -> Result<(), Error> {
            let _ = s.spawn(|| -> Result<(), Error> {
                if let Some(client) = &self.client {
                    client.disconnect(None)?;
                } else {
                    eprintln!("Error: client was None.")
                };
                Ok(())
            });
            Ok(())
        })?;
        Ok(())
    }

    fn send(&mut self, message: Message) -> Result<(), Error> {
        thread::scope(|s| -> Result<(), Error> {
            let _ = s.spawn(|| -> Result<(), Error> {
                if let Some(client) = &self.client {
                    let msg = mqtt::Message::new(message.topic, message.text, message.qos as i32);
                    client.publish(msg)?;
                } else {
                    eprintln!("Error: client was None.")
                };
                Ok(())
            });
            Ok(())
        })?;
        Ok(())
    }
}
